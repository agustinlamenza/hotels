'use strict';

const Boom = require('@hapi/boom');
const { Property } = require('../models/properties.model');

const getAll = async (request, h) => {

    const items = await Property.find({});

    return items;
};

const find = async (request, h) => {

    const items = await Property.findById(request.params.id);

    return items;
};

const add = async (request, h) => {

    const dto = request.payload;

    // TODO: validate payload

    const item = await Property.create({
        description: dto.description,
        latitude: dto.latitude,
        longitude: dto.longitude,
        area: dto.area
    });

    return item;
};

const remove = async (request, h) => {
    const item = await Property.deleteOne({ _id: request.params.id });

    return item;
};

module.exports = {
    getAll,
    find,
    add,
    remove
};