'use strict';

const { Search } = require('../models/search.model');

const search = (request, h) => {
    return 'search';
};

const test = async (request, h) => {

    const result = await Search.find({ city: 'florida1' });

    return result;
};

const insert = async (request, h) => {

    const item = await Search.create({
        city: request.params.city
    });

    return item;
};

module.exports = {
    search,
    test,
    insert
};