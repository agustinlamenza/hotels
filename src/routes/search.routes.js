'use strict';

const handlers = require('../handlers/search.handler');

const routes = [
    {
        method: 'GET',
        path: '/search',
        handler: handlers.search
    },
    {
        method: 'GET',
        path: '/test',
        handler: handlers.test
    },
    {
        method: 'GET',
        path: '/insert/{city}',
        handler: handlers.insert
    }
];

module.exports = routes;