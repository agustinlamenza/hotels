'use strict';

const handlers = require('../handlers/properties.handler');

const routes = [
    {
        method: 'GET',
        path: '/property',
        handler: handlers.getAll
    },
    {
        method: 'GET',
        path: '/property/{id}',
        handler: handlers.find
    },
    {
        method: 'POST',
        path: '/property',
        handler: handlers.add
    },
    {
        method: 'DELETE',
        path: '/property/{id}',
        handler: handlers.remove
    }
];

module.exports = routes;