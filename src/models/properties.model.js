'use strict';

const { model, Schema } = require('mongoose');

const propertySchema = new Schema({
    description: {
        type: String,
        required: true
    },
    latitude: Number,
    longitude: Number,
    area: Number
});

module.exports = {
    Property: model('Property', propertySchema)
};
