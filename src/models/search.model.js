'use strict';

const { Schema, model } = require('mongoose');

const searchSchema = new Schema({
    city: String
});

module.exports = {
    Search: model('Search', searchSchema)
};