'use strict';

require('dotenv').config();

const hapi = require('@hapi/hapi');
const mongoose = require('mongoose');
const searchRoutes = require('./routes/search.routes');
const propertyRoutes = require('./routes/properties.routes');

const configure = async () => {

    // Server
    const server = hapi.server({
        port: process.env.PORT,
        host: process.env.HOST
    });

    // Routes
    server.route(searchRoutes);
    server.route(propertyRoutes);

    // DB
    await mongoose.connect(process.env.MONGODB, {
        useNewUrlParser: true
    });

    // Plugins

};

const init = async () => {

    await server.initialize();

    return server;
};

const start = async () => {

    await server.start();

    console.log(`Server running at: ${server.info.uri}`);

    return server;
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

module.exports = {
    configure,
    init,
    start
};
