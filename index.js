'use strict';

const { configure, start } = require('./src/server');

configure();

start();